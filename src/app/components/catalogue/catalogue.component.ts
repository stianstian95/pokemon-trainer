import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { Pokemon } from "src/app/models/pokemon.model";
import { PokemonService } from "src/app/services/pokemon.service";
import { UserService } from "src/app/services/user.service";

@Component({
    selector: "app-catalogue-component",
    templateUrl: "./catalogue.component.html",
    styleUrls: ["./catalogue.component.css"]
})
export class CatalogueComponent implements OnInit {

    pokemonList: Pokemon[] | null = []; //List to hold all the pokemon objects that we get from the pokemon service
    public trainerPokemon: any = this.userService.user?.pokemon; //holds the names of the pokemon the user owns

    //Calls the userService logout function
    public onLogoutClick() {
        this.userService.logout()
    }

    constructor(private http: HttpClient, private userService: UserService, private readonly pokemonService: PokemonService) {
    }

    ngOnInit(): void {
        this.pokemonService.findAllPokemon() //Fetches all the pokemon and saves them to the service
        this.pokemonList = this.pokemonService.cataloguePokemon // assigns pokemonList to the currently retrieved pokemon from pokemonservice
    }

}