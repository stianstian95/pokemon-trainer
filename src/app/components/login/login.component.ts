import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { UserService } from "src/app/services/user.service";


@Component({
    selector: "app-login-component",
    templateUrl: "./login.component.html",
})
export class LoginComponent {

    private username: string = '';

    // submit func for the form
    public onSubmit(createUsername: NgForm): void {
        //get the username value from the form
        this.username = createUsername.value.username
        // use the login method in userService to login
        this.userService.login(this.username)        
    }

    constructor(private userService: UserService) {}
}
