import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CataloguePokeItemComponent } from './catalogue-poke-item.component';

describe('CataloguePokeItemComponent', () => {
  let component: CataloguePokeItemComponent;
  let fixture: ComponentFixture<CataloguePokeItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CataloguePokeItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CataloguePokeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
