
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-catalogue-poke-item',
  templateUrl: './catalogue-poke-item.component.html',
  styleUrls: ['./catalogue-poke-item.component.css']
})
export class CataloguePokeItemComponent implements OnInit {

  @Input() poke!: Pokemon; //pokemon inputted from parent
  @Input() trainerPokemon: any = []; //all the pokemon the user owns

  

  public owned: boolean = false //start of with assuming user don't own the pokemon

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.owned = this.trainerPokemon.indexOf(this.poke.name) !== -1 //Check if the user owns this pokemon and assigns the bool to truth of the statement
  }

  //Add pokemon to collection
  public onPokemonClick(poke: Pokemon): void {
    this.userService.addPokemon(poke.name) // call user service for adding pokemon, with this components pokemons name as argument
    this.owned = true // since it doesn't update instantly, assign this manually
  }

}
