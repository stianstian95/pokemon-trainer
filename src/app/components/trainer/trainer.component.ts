import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { PokemonService } from "src/app/services/pokemon.service";
import { UserService } from "src/app/services/user.service";
import { Pokemon } from "../../models/pokemon.model"

@Component({
    selector: "app-trainer-component",
    templateUrl: "./trainer.component.html",
    styleUrls: ["./trainer.component.css"],
})
export class TrainerComponent implements OnInit {

    public pokemon: Pokemon | null = null;

    public user: any = []; //anytype to hold the response

    myPokemon: any = [] // list of strings of the pokemon user owns
    pokemonList: Pokemon[] | null = []; // list of the pokemon objects the user 
    
    constructor(private http: HttpClient, private userService: UserService, private pokemonService: PokemonService) {
    }

    //Logout with userService method
    public onLogoutClick() {
        this.userService.logout()
    }

    ngOnInit() {
        if (this.userService.user?.pokemon){ //checks if the user has any pokemon
            this.myPokemon = this.userService.user.pokemon //if so assign them to myPokemon
        }
        this.pokemonService.findTrainerPokemon(this.myPokemon) //fetch these pokemon from API using myPokemon as argument
        this.pokemonList = this.pokemonService.trainerPokemon // assign pokmeonlist to the fetched pokemon in the pokemonservice
     }
}