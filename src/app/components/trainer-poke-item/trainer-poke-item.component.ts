import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-trainer-poke-item',
  templateUrl: './trainer-poke-item.component.html',
  styleUrls: ['./trainer-poke-item.component.css']
})
export class TrainerPokeItemComponent implements OnInit {

  @Input() poke!: Pokemon; //input of pokemon from trainer component

  constructor(private userService: UserService,) { }

  public released: boolean = false; //initially false

  ngOnInit(): void {
  }

  public onPokemonClick(poke: Pokemon): void {
    this.userService.removePokemon(poke.name); // use the userService method to release pokemon with this pokemons name
    this.released = true; //Display message that this pokemon was released
  }

}
