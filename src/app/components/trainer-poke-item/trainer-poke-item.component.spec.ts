import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerPokeItemComponent } from './trainer-poke-item.component';

describe('TrainerPokeItemComponent', () => {
  let component: TrainerPokeItemComponent;
  let fixture: ComponentFixture<TrainerPokeItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainerPokeItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainerPokeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
