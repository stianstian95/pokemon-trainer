import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { LoginComponent } from './components/login/login.component';
import { TrainerComponent } from './components/trainer/trainer.component';
import { CataloguePokeItemComponent } from './components/catalogue-poke-item/catalogue-poke-item.component';
import { TrainerPokeItemComponent } from './components/trainer-poke-item/trainer-poke-item.component';

@NgModule({
  declarations: [
    AppComponent,
    TrainerComponent,
    CatalogueComponent,
    LoginComponent,
    CataloguePokeItemComponent,
    TrainerPokeItemComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
