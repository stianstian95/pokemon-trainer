export interface User {
    id: number,
    username: String,
    pokemon: String[]
}

export interface registerUserResponse {
    data: User
    success: boolean,
    error?: string;
}