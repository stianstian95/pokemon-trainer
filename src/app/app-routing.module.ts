import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CatalogueComponent } from "./components/catalogue/catalogue.component";
import { LoginComponent } from "./components/login/login.component";
import { TrainerComponent } from "./components/trainer/trainer.component";
import { UserGuard } from "./guards/user-guard";


const routes: Routes = [{
    path: '',
    component: LoginComponent
    }, {
    path: 'catalogue',
    component: CatalogueComponent,
    canActivate: [ UserGuard ]
    },
    {
    path: 'trainer',
    component: TrainerComponent,
    canActivate: [ UserGuard ]
    }
]

@NgModule( {
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}