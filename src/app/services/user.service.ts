import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs';
import { User } from '../models/user.model';

const USER_KEY = 'pokemon-user';
const URL = 'https://noroff-asl-api.herokuapp.com/trainers';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient, private router: Router) {
    
    const storedUser: string = localStorage.getItem(USER_KEY) ?? ""; //From dewalds movie project
    if (storedUser) { //if there is a stored user, then assign it to _user 
      const json = JSON.parse(storedUser) as User;
      this._user = json;
      this.router.navigateByUrl('/catalogue'); // as you are logged in, you should be redirected to the catalogue
    }
  }

  private _user: User = { username: '', id: 0, pokemon: [] };

  private createHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-type': 'application/json',
      'x-api-key':
        'a8EQSTDhr6y6dya5Y_Sp?!9acwt.auS-5wX!5*HvLzL]h3F{[AhKDSzjh!?bg]Ww', // api key
    });
  }

  // getter for user
  get user(): User | undefined {
    return this._user;
  }
  //Function that logins the user, and if user does not exist, registers them
  // saves the user to this._user which is available to rest of app
  // saves the user to localstorage
  // redirects the user to catalogue when login finished
  public login(username: string): void {
    this.http
      .get<User[]>(`${URL}?username=${username}`) //gets the user with the provided username
      .pipe(
        finalize(() => { //redirect when finalizing
          this.router.navigateByUrl('/catalogue');
        })
      )
      .subscribe({
        next: (response: User[]) => {
          if (response[0]) { // if response is not an empty array the user exist and we register them
            this._user = response[0]; //user is the first (and only) item of the response array
            localStorage.setItem(USER_KEY,JSON.stringify(this._user)) // store user in localstorage
          }
          if (this._user.username === '') { //if username is (still) empty we need to register them to API instead of fetching them from API
            this.registerUser(username); //call the register function, this is a nested subscription, see explanation in readme
          }          
        },
        error: (error) => {
          console.log(error.message);
        },
      });
  }

  //function that logs user out, and resets _user and clears localstorage
  public logout(): void {
   localStorage.removeItem(USER_KEY);
   this._user = { username: '', id: 0, pokemon: [] };
   this.router.navigateByUrl('');
  }

  // registers user in API, and stores the user in _user and localstorage and redirects to catalogue
  private registerUser(username: string): void {
    //the user object we wish to register
    const user = {
      username: username,
      pokemon: [],
    };
    const headers = this.createHeaders();

    this.http
      .post<User>(URL, user, {
        headers,
      })
      .subscribe((response) => {// get a response of a userobject
        this._user = response; //saves userobject response to _user
        localStorage.setItem(USER_KEY,JSON.stringify(this._user)) // save user in localstorage
        this.router.navigateByUrl('/catalogue'); //redirect, this should be handled by login, but doesn't happen so need it here as well.
      });
  }
  // Updates the user to own a pokemon given as an argument
  // will update _user, localstorage and the API with the new owned pokemon
  public addPokemon(pokename: string): void {
    const pokemonList: String[] = [...this._user.pokemon, pokename]; //make a new list with the new and old pokemon
    const headers = this.createHeaders();
    this.http
      .patch<User>(
        `${URL}/${this._user.id}`,
        { pokemon: pokemonList },
        { headers }
      )
      .subscribe((response) => {
        this._user = response;//save new user object
        localStorage.setItem(USER_KEY,JSON.stringify(this._user))
      });
  }
  // Updates the user to not own a pokemon given as an argument
  // will update _user, localstorage and the API to be list of pokemon without removed pokemon
  public removePokemon(pokename: string): void {
    let pokemonList: String[] = [...this._user.pokemon]; //make a copy of currently owned pokemon
    pokemonList = pokemonList.filter((pokeString) => pokeString !== pokename); //filter out the pokemon that is to be removed
    const headers = this.createHeaders();
    this.http
      .patch<User>(
        `${URL}/${this._user.id}`,
        { pokemon: pokemonList }, //give argument of updated pokemonlist
        { headers }
      )
      .subscribe((response) => {
        this._user = response; //save new user object
        localStorage.setItem(USER_KEY,JSON.stringify(this._user))
      });
  }
}
