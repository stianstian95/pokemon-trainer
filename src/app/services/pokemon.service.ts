import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { UserService } from './user.service';

const pokemonURL: String = "https://pokeapi.co/api/v2/pokemon"
const amountPokemon: number = 151;

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private _cataloguePokemon: Pokemon[] = [];
  private _trainerPokemon: Pokemon[] = [];

  get cataloguePokemon(): Pokemon[] {
    return this._cataloguePokemon;
  }

  get trainerPokemon(): Pokemon[] {
    return this._trainerPokemon;
  }

  constructor(private http: HttpClient, private userService: UserService) { }

  // fetch catalogue pokemon
  findAllPokemon(): void {
    this._cataloguePokemon = [];
    this.http.get<any>(`${pokemonURL}?limit=${amountPokemon}`)
      .subscribe({
        next: (response) => {
          for (let index in response.results) {
            this.extractPokemon(response.results[index]);
          }
        },
        error: (error) => {
          console.log(error.message);
        }
      })
  }

  //Helper method to extract pokemon information and save it to a new pokemon object, 
  // and push it to the pokemonlist
  public extractPokemon = (pokeInfo: any) => {
    let poke: Pokemon = { name: '', id: 0, imgUrl: '' };
    poke.name = pokeInfo.name;
    poke.id = parseInt(pokeInfo.url.slice(34, -1))
    poke.imgUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${poke.id}.png`
    this._cataloguePokemon?.push(poke);
  }

  // fetch trainer pokemon (get which pokemon user own form user service)
  findTrainerPokemon(pokemon: string[]): void {
    this._trainerPokemon = [];
    for (let poke of pokemon) {
      const URL = `${pokemonURL}/${poke}`
      this.http.get<Pokemon>(URL)
        .subscribe({
          next: (response: Pokemon) => {

            let pokemon: Pokemon = response //assigns the pokemon to the response and match the relevant information
            //need to assign the imgurl manually as we don't get it in the response
            pokemon.imgUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.id}.png`
            this._trainerPokemon?.push(pokemon) //push the pokemon to the pokelist 
          },
          error: (error) => {
            console.log(error.message);
          }
        })
    }
  }
}