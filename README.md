# Pokemon Trainer App
heroku-url: https://dry-ridge-08243.herokuapp.com/

## Contributors:
Stian Selle - gitlab.com/stianstian95

Jonas Svåsand - gitlab.com/jsva

## Description:
This is an angular web app, that lets you browse Pokémon and add them to your collection. 

The Pokémon included in this app is restricted to the original 151 Pokémon. 

You can login with a username to save your process to our API server. Your username will be remembered by your browser, so you will not have to login each time you visit.  Upon login you can browse the catalogue and add Pokémon from the catalogue to your own collection. To see your collection visit the trainer page. On the trainer page you can see your Pokémon and release them if you do not want them in your collection any longer.


## Instructions

**You need to have npm installed**

1. clone this repo
2. navigate to the folder you cloned the project to and open a command prompt in this location
3. `npm install`
4. `ng serve`
5. Go to the the adress, provided in the command prompt, in your browser. (usually localhost port 4200)
6. You can now use the app.
7. Enter a user (can not be empty). Usernames are case sensitive, and unique 
8. You will be redirected to the Pokémon catalogue. Catch any pokemon you wish by using the 'Add to my collection" button. Pokemon you already own are indicated with a pokeball image in place of the button.
9. You can navigate to the trainer page with the inbuilt navigation bar. 
10. On the trainer page you will see all the pokemon you own. You can release them by using the button with text "release".

Alternatively you can use the app by visiting the website url provided at the top.


## Project status

- We forgot to add the figma compononent tree, we did create it before we started.

- There was no restriction in the assignment description on saving userID in the browser. We therefore chose to save the whole userobject including ID in the localStorage.
- There was no restriction in the assignment description or the API description on how to update users Pokémon in the userAPI. We therefore felt safe to overwrite the Pokémon attribute on user in the userAPI each time they added/removed a Pokémon. We do know that we should not do this in real production app, but chose to do this as there was no way to easily save earlier versions of our API.
- Pokémon are not instantly removed from the page when released. The removed Pokémon will first be removed from the page upon refresh, this is a restriction that came from our design, but we have purposely chosen to keep this, so that the page updating doesn't interfer the user trying to remove additional Pokémon.
- In the html of trainer-component-item and catalogue-component-item, we use two *ngIf statements that could have been combined to one if else statement. We tried to do this but did not get the wanted results. While we would like to use an else statement for simplicity (and efficiency?), we chose to focus or time on other more pressing problems.
- We are nesting some of our http requests (in login). This is bad practice and we know this, but did not succeed in using observables or pipe to get the desired effects and therefore chose to do it this way despite the bad practice/anti-pattern. 
- When you refresh and there is a saved user in localStorage, you will get logged in. When this happens you will always get redirected to the catalogue, even if you are on the trainer page. This is done based on our interpretation on the requirements from the assignment description.
- We added a logout button even though this was not mentioned in the assignment description. This is so that you can easily clear the browser storage and the current set user in the userService. 

